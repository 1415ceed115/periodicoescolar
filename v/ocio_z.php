<!DOCTYPE  >

<head>

  <title>Periódico Escolar</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <link href="../css/materialize.css" rel="stylesheet"  type="text/css">


  <script src="../js/jquery.min.js"></script>
  <script src="../js/materialize.min.js"></script>



</head>

<body>
  
   <?php include_once("../analyticstracking.php") ?>
  <!-- Abro div contenedor para conenerla web al 70-->





  <div class="container"> 

    <?php include 'nav.php'; /* Llamo a la barra de navegación */ ?>

    <br><br><br>

    <h3 class="titular">Ocio </h3>

    <div class="row s12">

      <div class="col s12 grey-text"><h5>Recomendaciones de ocio para la Semana  <?php
          echo date("W") . " de " . date("Y") .
          "</h5> <hr class='orange-text text-lighten-2'>";

          require '../c/ocio.php';
          ?>
          <hr class="orange-text text-lighten-2">
          </div>
          </div>


          <div class="row s12">

            <div class="col s2"><br><br><br>Selecciona semana diferente:
              <form method="post" action="../c/ocio.php">
                <input name="nsemana">
                <input type="submit" value="Ver">

              </form>
            </div>
            <div class="col s2"> &nbsp; &nbsp; </div>
            <div class="col s8"><br><br><br><br><br>En esta página encontrarás semanalmente ofertas de ocio
              diferente y cultural en televisión, cine e internet 
              seleccionados por periodico escolar
            </div>
          </div>





          <?php include 'footer.php'; /* Llamo al footer */ ?>


          <script>

            // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
            $(document).ready(function () {
              $('select').material_select(); // Para llamar al select
              $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
              $('.dropdown-button').dropdown(); // Llamo al menu desplegable
              $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
              $('.slider').slider(); // Llamo a la funcion del Slider

            });



          </script>


      </div>  <!-- Cierro el div container-->
      </body>
      </html>
