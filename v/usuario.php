<!DOCTYPE html >


<html>
  <head>

    <title>Periódico Escolar</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


    <!-- MSgDg0YO2wL-FmRvv_zIbPUsXFo -->
    <meta name="robots" content="index, follow" />

    <!-- <meta name="p:domain_verify" content="bb50e9396f97c65cee229e2e5de6a001"/> -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="periodico escolar, plataforma multimedia, periodico digital, plataforma digital, plataforma multimedia, web para televidion " />
    <meta name="revisit-after" content="3 days" />
    <meta name="description" content="periodico escolar, plataforma multimedia, periodico digital, plataforma digital " />
    <meta name="keywords" content="periodico escolar, plataforma multimedia, periodico digital, plataforma digital, plataforma multimedia, web para televidion " />
    <meta name="geo.region" content="ES-V" />
    <meta name="geo.placename" content="ALBORAYA" />

    <meta name="geo.position" content="39.500995; -0.34948" />
    <meta name="ICBM" content="39.500995, -0.34948" />



    <meta name="dcterms.language" CONTENT="ES">
    <meta name="dcterms.source" CONTENT="http://periodicoescolar.es/">
    <meta name="dcterms.relation" CONTENT="http://periodicoescolar.es">
    <meta name="dcterms.title" CONTENT="Periodico Escolar, plataforma multimedia">
     <meta name="dcterms.subject" CONTENT="diseño web Juan Antonio Bleda">
    <meta name="dcterms.description" CONTENT="Periodito escolar es una plataforma multimedia creada por Juan A. Bleda, diseñada para periodicos digitales y televisiones ">

    <link rel="shortcut icon" type="image/png" href="../images/favicon.png"/>


    <link href="../css/titular.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="../css/materialize.css" rel="stylesheet"  type="text/css">


    <script src="../js/jquery.min.js"></script>
    <script src="../js/materialize.min.js"></script>





  </head>

  <body>
 <?php include_once("../analyticstracking.php") ?>
    <div class="container">

      <?php
      include 'header.php';
      include 'nav.php';
      ?>
      <br><br>

      <div class="row 12">

        <div class='card-content darken-3-text col s12 '>

          <!-- El fetch y fectch all nos devuelve un array de objetos-->

          <h3 class='titular'><?php
            echo $usuarioCompleto[0]['nombre'] ."<br>";
            echo $usuarioCompleto[0]['apellidos'] ."<br>";
            echo $usuarioCompleto[0]['dni']."<br>";
            ?></h3>


          <h5 class='grey-text'>

            <?php
            echo $usuarioCompleto[0]['email'] . "<br>";
            echo $usuarioCompleto[0]['twitter'] . "<br>";
            echo $usuarioCompleto[0]['telefono'] . "<br>";
            echo $usuarioCompleto[0]['direccion'] . "<br>";
            echo $usuarioCompleto[0]['poblacion'] . "<br>";
            echo $usuarioCompleto[0]['provincia'] . "<br>";
            echo $usuarioCompleto[0]['codigopostal'] . "<br>";
            echo $usuarioCompleto[0]['pais'] . "<br>";
            echo $usuarioCompleto[0]['permiso'] . "<br>";
            echo $usuarioCompleto[0]['psw'] . "<br>";
            
            ?>



          </h5><br>



          <hr class="orange-text text-lighten-2"><br>



          <br><br><br>

<?php include 'footer.php'; /* Llamo al footer */ ?>

        </div>
        <script>

          // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
          $(document).ready(function () {
            $('select').material_select(); // Para llamar al select
            $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
            $('.dropdown-button').dropdown(); // Llamo al menu desplegable
            $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
            $('.slider').slider(); // Llamo a la funcion del Slider

          });



        </script>

      </div>
    </div>  <!-- Cierro el div container-->
  </body>
</html>

