<!DOCTYPE >

<html>


  <head>

    <title>Periódico Escolar</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="../css/materialize.css" rel="stylesheet"  type="text/css">


    <script src="../js/jquery.min.js"></script>
    <script src="../js/materialize.min.js"></script>



  </head>

  <body>
    <!-- Abro div contenedor para conenerla web al 70%-->
    <div class="container"> 


      <?php include 'nav.php'; /* Llamo a la barra de navegación */ ?>

      <br><br><br>

      <h3 class="titular">Eventos en directo </h3>

      <div class="row s12">

        <div class="col s12 grey-text"><h5>Aquí podrás ver los eventos en directo y los videos de los anteriores directos</h5>

          <hr class="orange-text text-lighten-2">
        </div>
      </div>


      <div class="row s12">

        <div class="col s12 grey-text"><h5></h5>
          
          <iframe src="https://embed.bambuser.com/channel/juanvlc" width="900" height="900" frameborder="0">Tu navegador no soporta iframes.</iframe>

<br> Haz click en la pantalla para ver mas opciones y los videos anteriores
          <br><br><br><br><br><br>
          <hr class="orange-text text-lighten-2">
        </div>
      </div>




      <?php include 'footer.php'; /* Llamo al footer */ ?>


      <script>

        // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
        $(document).ready(function () {
          $('select').material_select(); // Para llamar al select
          $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
          $('.dropdown-button').dropdown(); // Llamo al menu desplegable
          $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
          $('.slider').slider(); // Llamo a la funcion del Slider

        });



      </script>


    </div>  <!-- Cierro el div container-->
  </body>
</html>
