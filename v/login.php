<!DOCTYPE >

<head>

  <title>Periódico Escolar</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <link href="../css/materialize.css" rel="stylesheet"  type="text/css">


  <script src="../js/jquery.min.js"></script>
  <script src="../js/materialize.js"></script>



</head>

<body>
   <?php include_once("../analyticstracking.php") ?>
  <!-- Abro div contenedor para conenerla web al 70-->
  <div class="container"> 


    <?php include 'nav.php'; /* Llamo a la barra de navegación */ ?>


    <br><br><br>

    <h3 class="titular">Zona privada</h3>

    <div class="row s12">

      <div class="col s12 grey-text"><h5>Introduce tus datos para entrar</h5>
        <hr class="orange-text text-lighten-2">
      </div>
    </div>

    <div class="row s12">
   
      <div class="col s5">
        <form action="../c/ControladorPrincipal.php?accion=log"  method="post">


          Usuario   <input type="text" name="dni"> <br> 
          Contraseña <input type="text" name="psw"> <br>


          <input class="btn" type="reset" value="Limpiar datos">  <input class="btn" type="submit"  value="Entrar">

          </div>

          </div>


          <div class="row s12">
            <div class="col s12">
              <a href="../v/panelAdmin.php" class="btn" >Panel Administrador</a><br>

              <a href="../c/ControladorPrincipal.php?accion=nc">Crear noticia</a>
            </div>


          </div>




          <?php include 'footer.php'; /* Llamo al footer */ ?>


          <script>

            // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
            $(document).ready(function () {
              $('select').material_select(); // Para llamar al select
              $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
              $('.dropdown-button').dropdown(); // Llamo al menu desplegable
              $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
              $('.slider').slider(); // Llamo a la funcion del Slider

            });



          </script>


      </div>  <!-- Cierro el div container-->
      </body>
      </html>
