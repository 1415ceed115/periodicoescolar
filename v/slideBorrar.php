<!DOCTYPE >

<head>
    
    <title>Periódico Escolar</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link href="../css/materialize.css" rel="stylesheet"  type="text/css">
    
     
    <script src="../js/jquery.min.js"></script>
    <script src="../js/materialize.min.js"></script>
    
    
    
</head>

<body>
  <!-- Abro div contenedor para conenerla web al 70-->
  <div class="container"> 
  
  
<?php include 'nav.php'; /* Llamo a la barra de navegación*/?>

    
    <div class="row 12">
      <div class="col 8">
        
        
        <form action="../c/ControladorPrincipal.php?accion=sb"  method="post">
          <br><br><br>
          <h3 class="titular">Borrar Slide</h3> <br>
          <h5 class="grey-text">Introduce id del slide</h5>
          <input type="text" name="idslide">
          <input class="btn red" type="submit" name="Borrar slide" value="Borrar slide">
          
          
        </form>
      
      
      </div>
      
      <div class="col 4">  </div>
    </div>
    
    
     <div class="row 12">
      <div class="col 4">  </div>
      <div class="col 4">  </div>
      <div class="col 4"><br><br>En esta página podrás borrar el slide de periodico escolar que le indiques</div>
    </div>
    
    

    
    
<?php include 'footer.php'; /* Llamo al footer */ ?>
    

<script>
  
  // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
  $(document).ready(function(){
    $('select').material_select(); // Para llamar al select
    $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
    $('.dropdown-button').dropdown(); // Llamo al menu desplegable
    $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
    $('.slider').slider(); // Llamo a la funcion del Slider
    
  });
  
  
  
</script>
  

</div>  <!-- Cierro el div container-->
</body>
</html>
