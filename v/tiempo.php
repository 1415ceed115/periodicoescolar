<!DOCTYPE >

<head>

  <title>Periódico Escolar</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



  <link href="../css/materialize.css" rel="stylesheet" type="text/css"/>
  <script src="../js/jquery.min.js"></script>
  <script src="../js/materialize.min.js"></script>



</head>

<body>
   <?php include_once("../analyticstracking.php") ?>
  <!-- Abro div contenedor para conenerla web al 70-->
  <div class="container"> 


    <?php include 'nav.php'; /* Llamo a la barra de navegación */ ?>
    <br>
    <center> - Periódico Escolar - Instituto Santa Tecla -</center>
    <br>

    <h3 class="titular">El tiempo</h3>

    <div class="row s12">

      <div class="col s12 grey-text"><h5>Previsiones meteorlógicas para los próximos dias</h5>
          <hr class="orange-text text-lighten-2">
          </div>
          </div>


          <div class="row s12">
           
            <div class="col s12"> 
              <script type='text/javascript' src='http://www.aemet.es/es/eltiempo/prediccion/municipios/launchwidget/valencia-id46250?w=g4p01110001ohmffffffw900z250x4f86d9t95b6e9r1s8n2'></script>
              <noscript><a target='_blank' style='font-weight: bold;font-size: 1.20em;' href='http://www.aemet.es/es/eltiempo/prediccion/municipios/valencia-id46250'>
                El Tiempo. Consulte la predicción de la AEMET para Valencia</a></noscript>
            </div>
     
          </div>


          <div class="row s12">
             
            <div class="col s12 grey-text">Información avalada por el instituto nacional de meterología AEMET para periodico escolar<br><br>
            <hr class="orange-text text-lighten-2">
            </div>
          </div>




          <?php include 'footer.php'; /* Llamo al footer */ ?>


          <script>

            // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
            $(document).ready(function () {
              $('select').material_select(); // Para llamar al select
              $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
              $('.dropdown-button').dropdown(); // Llamo al menu desplegable
              $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
              $('.slider').slider(); // Llamo a la funcion del Slider

            });



          </script>


      </div>  <!-- Cierro el div container-->
      </body>
      </html>
