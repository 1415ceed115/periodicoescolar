<!DOCTYPE >

<head>

  <title>Periódico Escolar</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <link href="../css/materialize.css" rel="stylesheet"  type="text/css">


  <script src="../js/jquery.min.js"></script>
  <script src="../js/materialize.min.js"></script>



</head>

<body>
  
   <?php include_once("../analyticstracking.php") ?>
  <!-- Abro div contenedor para conenerla web al 70-->
  <div class="container"> 


    <?php include 'nav.php'; /* Llamo a la barra de navegación */ ?>


    <div class="row s12">
      <div class="col s1"> &nbsp; </div>
      <div class="col s10">

        <form action="../c/ControladorPrincipal.php?accion=nv"  method="post">
          <br><br><br>
          <h3 class="titular">Buscar noticia</h3>
          <hr class="orange-text text-lighten-2">
          <h5 class="grey-text">Introduce id de noticia</h5>

          <input type="text" name="idnoticia"><br>
          <input class="btn teal" type="submit" name="Buscar noticia" value="Buscar noticia">


        </form>

      </div>

      <div class="col s5">  </div>
    </div>


    <div class="row s12">
      <div class="col s4">  </div>
      <div class="col s4">  </div>
      <div class="col s4"><br><br>En esta página podrás buscar las noticias de periodico escolar.</div>
    </div>





    <?php include 'footer.php'; /* Llamo al footer */ ?>


    <script>

      // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
      $(document).ready(function () {
        $('select').material_select(); // Para llamar al select
        $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
        $('.dropdown-button').dropdown(); // Llamo al menu desplegable
        $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
        $('.slider').slider(); // Llamo a la funcion del Slider

      });



    </script>


  </div>  <!-- Cierro el div container-->
</body>
</html>
