<!DOCTYPE >

<head>
    
    <title>Periódico Escolar</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <link href="../css/materialize.css" rel="stylesheet"  type="text/css">
    
     
    <script src="../js/jquery.min.js"></script>
    <script src="../js/materialize.min.js"></script>
    
    
    
</head>

<body>
   <?php include_once("../analyticstracking.php") ?>
  <!-- Abro div contenedor para conenerla web al 70-->
  <div class="container"> 
  
  
<?php include 'nav.php'; /* Llamo a la barra de navegación*/?>

    
    <br><br><br>

    <h3 class="titular">Datos de contacto</h3>

      <div class="row s12">

      <div class="col s12 grey-text">
        <h5> 619452323  info@periodicoescolar.es - juanbledap@gmail.com</h5>
          <hr class="orange-text text-lighten-2">
          <br><br>
          </div>
     </div>
    
    <div class="row s12">

      <div class="col s12 grey-text"><h5>Petición de información</h5>
          <hr class="orange-text text-lighten-2">
          
           <form action="../c/ControladorPrincipal.php?accion=contacto"  method="post">


        Nombbre   <input type="text" name="nombre"> <br> 
        Apellidos <input type="text" name="apellidos"> <br>
        Email <input type="text" name="email"> <br>
        Teléfono <input type="text" name="telefono"> <br>
        ¿De que quieres que te informemos? <input type="text" name="peticioninfo"> <br>
       

        <input class="btn" type="reset" value="Limpiar datos">  <input class="btn" type="submit"  value="solicitar Información">


      </form>

          
          
          
          </div>
     </div>
    
     <div class="row s12">

      <div class="col s12 grey-text"><h5>Localización</h5>
          <hr class="orange-text text-lighten-2">
          
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6157.368582823286!2d-0.35592742165311064!3d39.49904291792934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd60463d7931a313%3A0x17118901ff5fdd4a!2sAlboraya%2C+Valencia!5e0!3m2!1ses!2ses!4v1459201910017" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
     </div>
    
    
   
    

    
    
<?php include 'footer.php'; /* Llamo al footer */ ?>
    

<script>
  
  // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
  $(document).ready(function(){
    $('select').material_select(); // Para llamar al select
    $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
    $('.dropdown-button').dropdown(); // Llamo al menu desplegable
    $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
    $('.slider').slider(); // Llamo a la funcion del Slider
    
  });
  
  
  
</script>
  

</div>  <!-- Cierro el div container-->
</body>
</html>
