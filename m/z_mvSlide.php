<?php

//require "../c/conexion.php";
require '../m/mSlide.php';

class MvSlide {

  public function mvSlide() {


    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    // parent::__construct();
  }

  // Metodo para pedir una consulta y devolver registros
  public static function get_slides($idslide) {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    $consulta = $conexion_db->prepare('SELECT * FROM slide WHERE idslide="' . $idslide . '"');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $slides = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $slides; // Nos devuelve el array 

    /*
      return new noticias($datoarray['idnoticia'],$datoarray['titular'],$datoarray['fechanoticia'],$datoarray['subtitulo'],
      $datoarray['entradilla'],$datoarray['cuerpo'],$datoarray['imagen'],$datoarray['pieimagen'],$datoarray['observaciones'],$datoarray['categoria']
      ,$datoarray['tags'],$datoarray['fuente']
      );
     */
  }

  // Cierro constructor
//*

  public static function crearSlide($idslide) {


    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    //   O P C I O N     ( A )
     if (self::get_slides($idslide)) { // Actualiza si existe
    //if (self::get_noticia(get_noticia::getIdnoticia())) {

      $consultasql = $conexion_db->prepare("UPDATE  slide SET idslide= :idslide, s_idnoticia= :idnoticia, s_titular= :titular, s_imagen= :imagen,
                WHERE idslide=:idslide");

      $consultasql->bindParam(':idslide', slide::getIdslide());
      $consultasql->bindParam(':idnoticia', slide::getIdnoticia());
      $consultasql->bindParam(':titular', slide::getTitular());
      $consultasql->bindParam(':imagen', slide::getImagen());
  

      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al Actualizar un slide</p></script>";
        print $e->getMessage();
        exit();
      }
    } else {  // Si no existe inserta Slide
      $consultasql = $conexion_db->prepare(" INSERT INTO  slide (idslide, s_idnoticia, s_titular, s_imagen)
      VALUES (:idslide, :idnoticia, :titular, :imagen, ) ");

      $consultasql->bindParam(':idslide', slide::getIdslide());
      $consultasql->bindParam(':idnoticia', slide::getIdnoticia());
      $consultasql->bindParam(':titular', slide::getTitular());
      $consultasql->bindParam(':imagen', slide::getImagen());
  



      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al crear un Slide</p></script>";
        print $e->getMessage();
        exit();
      }
      Slide::setIdslide($conexion_db->lastInsertId());
    }
  }

  public static function borrarSlide($idslide) {

    $conectar = new Conexion();

    $conexion_db = $conectar->conexion();

    $consultasql = $conexion_db->prepare('DELETE FROM slide WHERE idslide="' . $idslide . '"');

    $consultasql->bindParam(':idslide', Slide::getIdslide());

    $consultasql->execute();
    //    echo "El Slide: ".$idslide."ha sido borrado";


    // Comprobar que el slide ha sido borrado NO FUNCIONA AÚN
    
    
    if($consultasql->execute()) {

      echo "<br> El slide: " . $idslide . " ha sido borrado";
    } else {
      print"<br> Error al borrar el slide " . $idslide;
    }
  }

// Cierro Borrar noticia


  /* C I E R R O    O P C I O N   (A) M V C
   */






  // echo "Había que desconecar";
  // self::desconectar($conexion_db);


  /*   O P C I O N     ( B ) sin M V C




    $consultasql = " INSERT INTO  noticias (idnoticia, titular, fechanoticia, subtitulo, entradilla, cuerpo, imagen, pieimagen, observaciones, categoria, tags, fuente)
    VALUES (:idnoticia, :titular, :fechanoticia, :subtitulo, :entradilla, :cuerpo, :imagen, :pieimagen, :observaciones, :categoria, :tags, :fuente) ";



    $resultado = $conexion_db->prepare($consultasql);

    if ($resultado->execute(array(":idnoticia" => $idnoticia, ":titular" => $titular, ":fechanoticia" => $fechanoticia, ":subtitulo" => $subtitulo,
    ":entradilla" => $entradilla, ":cuerpo" => $cuerpo, ":imagen" => $imagen, ":pieimagen" => $pieimagen, ":observaciones" => $observaciones,
    ":categoria" => $categoria, ":tags" => $tags, ":fuente" => $fuente))) {
    print "<br> La noticia se ha creado correctamente";
    } else {
    print "<br> Error al crear la noticia";
    }
   *  C I E R R R O   O P C I O N  ( B ) 
   */


// Cierro Clase  noticia
}

// Cierro Clase 
?>
