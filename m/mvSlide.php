<?php

require_once "../c/conexion.php";


require_once '../m/mSlide.php';

class MvSlide {

  public function mvSlide() {


    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    // parent::__construct();
  }

  // Metodo para pedir una consulta y devolver registros
  public static function get_slides($idslide) {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    $consulta = $conexion_db->prepare('SELECT * FROM slide WHERE idslide="' . $idslide . '"');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $slide = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $slide; // Nos devuelve el array de toda la 
    
     //$conexion_db = $conectar->desconectar($conexion);

    /*
      return new noticias($datoarray['idnoticia'],$datoarray['titular'],$datoarray['fechanoticia'],$datoarray['subtitulo'],
      $datoarray['entradilla'],$datoarray['cuerpo'],$datoarray['imagen'],$datoarray['pieimagen'],$datoarray['observaciones'],$datoarray['categoria']
      ,$datoarray['tags'],$datoarray['fuente']
      );
     */
  }

  

  // Metodo para pedir una consulta y devolver registros
  public static function getSlideEntero() {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    //$consulta = $conexion_db->prepare("SELECT * FROM slide WHERE idslide='s01' & 's02' & 's03");
        $consulta = $conexion_db->prepare('SELECT * FROM slide WHERE idslide="' . $idslide . '"');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $slide = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $slide; // Nos devuelve el array de toda la 
    
    print_r($slide);
    
     //$conexion_db = $conectar->desconectar($conexion);

   
      //return new slide($datoarray['idslide'],$datoarray['s_idnoticia'],$datoarray['s_titular'],$datoarray['s_imagen']);
       
  }

  
  
  /*
  // Metodo para pedir una consulta y devolver registros
  public static function getSlideEntero() {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    $consulta1 = $conexion_db->prepare("SELECT * FROM slide WHERE idslide='s01'");
     //   $consulta = $conexion_db->prepare('SELECT * FROM slide WHERE idslide="' . $idslide . '"');

    // Ejecuto la instruccion sql
    $consulta1->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $slide1 = $consulta1->fetchAll(PDO::FETCH_ASSOC);

    return $slide1; // Nos devuelve el array de toda la 
    
    print_r($slide1);
/*
    
      // Hago un prepare, para preparar la ejecución sql

    $consulta2 = $conexion_db->prepare("SELECT * FROM slide WHERE idslide='s02'");
     //   $consulta = $conexion_db->prepare('SELECT * FROM slide WHERE idslide="' . $idslide . '"');

    // Ejecuto la instruccion sql
    $consulta2->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $slide2 = $consulta2->fetchAll(PDO::FETCH_ASSOC);

    return $slide2; // Nos devuelve el array de toda la 
    
    print_r($slide2);
    
    
    
    
      // Hago un prepare, para preparar la ejecución sql

    $consulta3 = $conexion_db->prepare("SELECT * FROM slide WHERE idslide='s03");
     //   $consulta = $conexion_db->prepare('SELECT * FROM slide WHERE idslide="' . $idslide . '"');

    // Ejecuto la instruccion sql
    $consulta3->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $slide3 = $consulta3->fetchAll(PDO::FETCH_ASSOC);

    return $slide3; // Nos devuelve el array de toda la 
    
    print_r($slide3);
    
    
    
    
    
    
    
    
    
     //$conexion_db = $conectar->desconectar($conexion);

   
      //return new slide($datoarray['idslide'],$datoarray['s_idnoticia'],$datoarray['s_titular'],$datoarray['s_imagen']);
   
  }
  */
  
  
  // Cierro constructor
//*

  // Método para crear slides
  
  public static function crearSlide($idslide) {


    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    //   O P C I O N     ( A )
    if (self::get_slides($idslide)) { // Actualiza si existe
      // if (self::get_slides(get_slide::getIdslide())) {
      $consultasql = $conexion_db->prepare("UPDATE  slide SET idslide=:idslide, s_idnoticia=:s_idnoticia, s_titular=:s_titular,  s_imagen= :s_imagen ");


      $consultasql->bindParam(':idslide', Slide::getIdslide());
      $consultasql->bindParam(':s_idnoticia', Slide::getS_idnoticia());
      $consultasql->bindParam(':s_titular', Slide::getS_titular());
      $consultasql->bindParam(':s_imagen', Slide::getS_imagen());

      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al actualizar un Slide</p></script>";
        print $e->getMessage();
        exit();
      }
    } else {  // Si no existe inserta noticia
      $consultasql = $conexion_db->prepare(" INSERT INTO slide (idslide, s_idnoticia, s_titular, s_imagen)
      VALUES (:idslide,:s_idnoticia, :s_titular, :s_imagen ) ");


      $consultasql->bindParam(':idslide', Slide::getIdslide());
      $consultasql->bindParam(':s_idnoticia', Slide::getS_idnoticia());
      $consultasql->bindParam(':s_titular', Slide::getS_titular());
      $consultasql->bindParam(':s_imagen', Slide::getS_imagen());



      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al crear un slide</p></script>";
        print $e->getMessage();
        exit();
      }
      slide::setIdslide($conexion_db->lastInsertId());
    }
    //$desconectar = $conectar->desconectar($conexion_db);
    
      //$conexion_db = $conectar->desconectar($conexion);
  }

  
  public static function actualizarSlide($idslide) {


    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    //   O P C I O N     ( A )
  
      $consultasql = $conexion_db->prepare("UPDATE slide SET s_idnoticia=:s_idnoticia,s_titular=:s_titular,:s_imagen=s_imagen WHERE idslide=:idslide");


      
      $consultasql->bindParam(':idslide', Slide::getIdslide());
      $consultasql->bindParam(':s_idnoticia', Slide::getS_idnoticia());
      $consultasql->bindParam(':s_titular', Slide::getS_titular());
      $consultasql->bindParam(':s_imagen', Slide::getS_imagen());

      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al actualizar un Slide</p></script>";
        print $e->getMessage();
        exit();
      }
     slide::setIdslide($conexion_db->lastInsertId());


      }
     
    
    //$desconectar = $conectar->desconectar($conexion_db);
    
      //$conexion_db = $conectar->desconectar($conexion);
  

  
  
  
  public static function borrarSlide($idslide) {

    $conectar = new Conexion();

    $conexion_db = $conectar->conexion();

    $consultasql = $conexion_db->prepare('DELETE FROM slide WHERE idslide="' . $idslide . '"');

    $consultasql->bindParam(':idslide', slide::getIdslide());

    $consultasql->execute();
    //    echo "El Slide: ".$idslide."ha sido borrado";
    // Comprobar que el slide ha sido borrado NO FUNCIONA AÚN


    if ($consultasql->execute()) {

      echo "<br> El slide: " . $idslide . " ha sido borrado";
    } else {
      print"<br> Error al borrar el slide " . $idslide;
    }
 //$conexion_db = $conectar->desconectar($conexion);
  }

// Cierro Borrar noticia


  /* C I E R R O    O P C I O N   (A) M V C
   */






  // echo "Había que desconecar";
  // self::desconectar($conexion_db);


  /*   O P C I O N     ( B ) sin M V C




    $consultasql = " INSERT INTO  noticias (idnoticia, titular, fechanoticia, subtitulo, entradilla, cuerpo, imagen, pieimagen, observaciones, categoria, tags, fuente)
    VALUES (:idnoticia, :titular, :fechanoticia, :subtitulo, :entradilla, :cuerpo, :imagen, :pieimagen, :observaciones, :categoria, :tags, :fuente) ";



    $resultado = $conexion_db->prepare($consultasql);

    if ($resultado->execute(array(":idnoticia" => $idnoticia, ":titular" => $titular, ":fechanoticia" => $fechanoticia, ":subtitulo" => $subtitulo,
    ":entradilla" => $entradilla, ":cuerpo" => $cuerpo, ":imagen" => $imagen, ":pieimagen" => $pieimagen, ":observaciones" => $observaciones,
    ":categoria" => $categoria, ":tags" => $tags, ":fuente" => $fuente))) {
    print "<br> La noticia se ha creado correctamente";
    } else {
    print "<br> Error al crear la noticia";
    }
   *  C I E R R R O   O P C I O N  ( B ) 
   */


// Cierro Clase  noticia
}

// Cierro Clase 
?>
