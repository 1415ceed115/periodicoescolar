<?php

//require "../c/conexion.php";

class Usuario {

  // Defino las private variables del usuario
  private static $dni;
  private static $nombre;
  private static $apellidos;
  private static $email;
  private static $twitter;
  private static $telefono;
  private static $direccion;
  private static $poblacion;
  private static $provincia;
  private static $codigopostal;
  private static $pais;
  private static $permiso;
  private static $psw;

  
   // Defino el metodo constructor hago como un set inicial para no dejar vacio y evitar errores

  public function __construct($dni = null, $nombre = null, $apellidos = NULL, $email= NULL, $twitter = NULL, $telefono = NULL, 
          $direccion = NULL, $poblacion = NULL, $provincia = NULL, $codigopostal = NULL, $pais = NULL, $permiso = null, $psw=NULL
  ) {
    self::$dni = $dni;
    self::$nombre = $nombre;
    self::$apellidos = $apellidos;
    self::$email = $email;
    self::$twitter = $twitter;
    self::$telefono = $telefono;
    self::$direccion = $direccion;
    self::$poblacion = $poblacion;
    self::$provincia = $provincia;
    self::$codigopostal = $codigopostal;
    self::$pais = $pais;
    self::$permiso = $permiso;
    self::$psw = $psw;
    
    
    
  } 
  
  
    // Metodo getter  ponemos self::$variable en vez de this por trajabar con objetos
  
  function getDni() {
    return self::$dni;
  }

  function getNombre() {
    return self::$nombre;
  }

  function getApellidos() {
    return self::$apellidos;
  }

  function getEmail() {
    return self::$email;
  }

  function getTwitter() {
    return self::$twitter;
  }

  function getTelefono() {
    return self::$telefono;
  }

  function getDireccion() {
    return self::$direccion;
  }

  function getPoblacion() {
    return self::$poblacion;
  }

  function getProvincia() {
    return self::$provincia;
  }

  function getCodigopostal() {
    return self::$codigopostal;
  }

  function getPais() {
    return self::$pais;
  }

  function getPermiso() {
    return self::$permiso;
  }

  function getPsw() {
    return self::$psw;
  }

    // Metodos Setter
  
  
  function setDni($dni) {
   
    self::$dni = $dni;
  }

  function setNombre($nombre) {
    self::$nombre = $nombre;
  }

  function setApellidos($apellidos) {
    self::$apellidos = $apellidos;
  }

  function setEmail($email) {
    self::$email = $email;
  }

  function setTwitter($twitter) {
    self::$twitter = $twitter;
  }

  function setTelefono($telefono) {
    self::$telefono = $telefono;
  }

  function setDireccion($direccion) {
    self::$direccion = $direccion;
  }

  function setPoblacion($poblacion) {
    self::$poblacion = $poblacion;
  }

  function setProvincia($provincia) {
    self::$provincia = $provincia;
  }

  function setCodigopostal($codigopostal) {
    self::$codigopostal = $codigopostal;
  }

  function setPais($pais) {
    self::$pais = $pais;
  }

  function setPermiso($permiso) {
    self::$permiso = $permiso;
  }

  function setPsw($psw) {
    self::$psw = $psw;
  }

    





  /*
    // Metodo para pedir una consulta y devolver registros
    public function get_noticias($semana) {


    // La privateiable query extiende de la superclase Conexion
    $resultado = self::$conexion_db->query('SELECT * FROM ocio WHERE semana="' . $semana . '"');
    $noticias = $resultado->fetch_all(MYSQLI_ASSOC);

    return $noticias; // Nos devuelve el array
    }
   */
}

// Cierro Clase 
?>
