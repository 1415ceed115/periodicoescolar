<?php

require "../c/conexion.php";

class Comentarios extends Conexion {

  // Defino las privateiables de la noticia
  private $idcomentario;
  private $idnoticia;
  private $comentarista;
  private $comentario;
  private $fechacomentario;

  // Defino el metodo constructor
  public function comentarios() {

    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    parent::__construct();
  }

  // Cierro constructor
  // Metodos getter

  function getIdcomentario() {
    return $this->idcomentario;
  }

  function getIdnoticia() {
    return $this->idnoticia;
  }

  function getComentarista() {
    return $this->comentarista;
  }

  function getComentario() {
    return $this->comentario;
  }

  function getFechacomentario() {
    return $this->fechacomentario;
  }

  // Metodos Setter



  function setIdcomentario($idcomentario) {
    $this->idcomentario = $idcomentario;
  }

  function setIdnoticia($idnoticia) {
    $this->idnoticia = $idnoticia;
  }

  function setComentarista($comentarista) {
    $this->comentarista = $comentarista;
  }

  function setComentario($comentario) {
    $this->comentario = $comentario;
  }

  function setFechacomentario($fechacomentario) {
    $this->fechacomentario = $fechacomentario;
  }

  /*
    // Metodo para pedir una consulta y devolver registros
    public function get_noticias($semana) {


    // La privateiable query extiende de la superclase Conexion
    $resultado = $this->conexion_db->query('SELECT * FROM ocio WHERE semana="' . $semana . '"');
    $noticias = $resultado->fetch_all(MYSQLI_ASSOC);

    return $noticias; // Nos devuelve el array
    }
   */
}

// Cierro Clase 
?>
