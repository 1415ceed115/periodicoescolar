<?php

class Noticia {

  // Defino las privateiables de la noticia
  private static $idnoticia;
  private static $titular;
  private static $fechanoticia;
  private static $subtitulo;
  private static $entradilla;
  private static $cuerpo;
  private static $imagen;
  private static $pieimagen;
  private static $observaciones;
  private static $tags;
  private static $categoria;
  private static $fuente;

  // Defino el metodo constructor hago como un set inicial para no dejar vacio y evitar errores

  public function __construct($idnoticia = null, $titular = null, $fechanoticia = NULL, $subtitulo = NULL, $entradilla = NULL, $cuerpo = NULL, $imagen = NULL, $pieimagen = NULL, $observaciones = NULL, $tags = NULL, $categoria = NULL, $fuente = null
  ) {
    self::$idnoticia = $idnoticia;
    self::$titular = $titular;
    self::$fechanoticia = $fechanoticia;
    self::$subtitulo = $subtitulo;
    self::$entradilla = $entradilla;
    self::$cuerpo = $cuerpo;
    self::$imagen = $imagen;
    self::$pieimagen = $pieimagen;
    self::$observaciones = $observaciones;
    self::$tags = $tags;
    self::$categoria = $categoria;
    self::$fuente = $fuente;
  }

  // Metodo getter  ponemos self::$variable en vez de this por trajabar con objetos

  public static function getIdnoticia() {
    return self::$idnoticia;
  }

  public static  function getTitular() {
    return self::$titular;
  }

  public static  function getFechanoticia() {
    return self::$fechanoticia;
  }

  public static  function getSubtitulo() {
    return self::$subtitulo;
  }

  public static  function getEntradilla() {
    return self::$entradilla;
  }

  public static  function getCuerpo() {
    return self::$cuerpo;
  }

  public static  function getImagen() {
    return self::$imagen;
  }

  public static  function getPieimagen() {
    return self::$pieimagen;
  }

  public static  function getObservaciones() {
    return self::$observaciones;
  }

  public static  function getTags() {
    return self::$tags;
  }

  public static  function getFuente() {
    return self::$fuente;
  }

  public static  function getCategoria() {
    return self::$categoria;
  }

  // Metodo Setter

    public static function setIdnoticia($idnoticia) {
    self::$idnoticia = $idnoticia;
  }

    public static function setTitular($titular) {
    self::$titular = $titular;
  }

    public static function setFechanoticia($fechanoticia) {
    self::$fechanoticia = $fechanoticia;
  }

   public static function setSubtitulo($subtitulo) {
    self::$subtitulo = $subtitulo;
  }

    public static function setEntradilla($entradilla) {
   self::$entradilla = $entradilla;
  }

    public static function setCuerpo($cuerpo) {
   self::$cuerpo = $cuerpo;
  }

   public static function setImagen($imagen) {
    self::$imagen = $imagen;
  }

    public static function setPieimagen($pieimagen) {
    self::$pieimagen = $pieimagen;
  }

    public static function setObservaciones($observaciones) {
    self::$observaciones =$observaciones;
  }

    public static function setCategoria($categoria) {
    self::$categoria = $categoria;
  }

    public static function setTags($tags) {
    self::$tags = $tags;
  }

    public static function setFuente($fuente) {
    self::$fuente = $fuente;
  }

  /*
    // Metodo para pedir una consulta y devolver registros
    public function get_noticias($datoid) {


    // La privateiable query extiende de la superclase Conexion
    $resultado = $this->conexion_db->query('SELECT * FROM noticias WHERE idnoticia="' . $datoid . '"');
    $noticias = $resultado->fetch_all(MYSQLI_ASSOC);

    return $noticias; // Nos devuelve el array
    }
   */
}

// Cierro Clase 
?>
