<?php

//require "../c/conexion.php";
require '../m/mNoticia.php';

class MvNoticia {

  public function mvNoticia() {


    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    // parent::__construct();
  }

  // Metodo para pedir una consulta y devolver registros
  public static function get_noticias($datoid) {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    $consulta = $conexion_db->prepare('SELECT * FROM noticias WHERE idnoticia="' . $datoid . '"');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $noticias = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $noticias; // Nos devuelve el array 

    /*
      return new noticias($datoarray['idnoticia'],$datoarray['titular'],$datoarray['fechanoticia'],$datoarray['subtitulo'],
      $datoarray['entradilla'],$datoarray['cuerpo'],$datoarray['imagen'],$datoarray['pieimagen'],$datoarray['observaciones'],$datoarray['categoria']
      ,$datoarray['tags'],$datoarray['fuente']
      );
     */
  }

  
    public static function verPanel() {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    $consulta = $conexion_db->prepare('SELECT idnoticia,fechanoticia,titular,imagen FROM noticias ORDER BY fechanoticia DESC LIMIT 3');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $panelnoticias = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $panelnoticias; // Nos devuelve el array 

    print_r($panelnoticias);
    
    /*
      return new noticias($datoarray['idnoticia'],$datoarray['titular'],$datoarray['fechanoticia'],$datoarray['subtitulo'],
      $datoarray['entradilla'],$datoarray['cuerpo'],$datoarray['imagen'],$datoarray['pieimagen'],$datoarray['observaciones'],$datoarray['categoria']
      ,$datoarray['tags'],$datoarray['fuente']
      );
     */
  }
  
  
  
  
  // Cierro constructor
//*

  public static function crearNoticia($idnoticia) {


    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    //   O P C I O N     ( A )
     if (self::get_noticias($idnoticia)) { // Actualiza si existe
    //if (self::get_noticia(get_noticia::getIdnoticia())) {

      $consultasql = $conexion_db->prepare("UPDATE  noticias SET idnoticia= :idnoticia, titular= :titular, fechanoticia= :fechanoticia,
      subtitulo= :subtitulo, entradilla= :entradilla, cuerpo= :cuerpo, imagen= :imagen, pieimagen= :pieimagen, observaciones= :observaciones,
      categoria= :categoria, tags= :tags, fuente= :fuente WHERE idnoticia=:idnoticia");

      $consultasql->bindParam(':idnoticia', noticia::getIdnoticia());
      $consultasql->bindParam(':titular', noticia::getTitular());
      $consultasql->bindParam(':fechanoticia', noticia::getFechanoticia());
      $consultasql->bindParam(':subtitulo', noticia::getSubtitulo());
      $consultasql->bindParam(':entradilla', noticia::getEntradilla());
      $consultasql->bindParam(':cuerpo', noticia::getCuerpo());
      $consultasql->bindParam(':imagen', noticia::getImagen());
      $consultasql->bindParam(':pieimagen', noticia::getPieimagen());
      $consultasql->bindParam(':observaciones', noticia::getObservaciones());
      $consultasql->bindParam(':categoria', noticia::getCategoria());
      $consultasql->bindParam(':tags', noticia::getTags());
      $consultasql->bindParam(':fuente', noticia::getFuente());


      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al Actualizar una noticia</p></script>";
        print $e->getMessage();
        exit();
      }
    } else {  // Si no existe inserta noticia
      $consultasql = $conexion_db->prepare(" INSERT INTO  noticias (idnoticia, titular, fechanoticia, subtitulo, entradilla, cuerpo, imagen, pieimagen, observaciones, categoria, tags, fuente)
      VALUES (:idnoticia, :titular, :fechanoticia, :subtitulo, :entradilla, :cuerpo, :imagen, :pieimagen, :observaciones,:categoria, :tags, :fuente) ");


      $consultasql->bindParam(':idnoticia', noticia::getIdnoticia());
       $consultasql->bindParam(':titular', noticia::getTitular());
      $consultasql->bindParam(':fechanoticia', noticia::getFechanoticia());
      $consultasql->bindParam(':subtitulo', noticia::getSubtitulo());
      $consultasql->bindParam(':entradilla', noticia::getEntradilla());
      $consultasql->bindParam(':cuerpo', noticia::getCuerpo());
      $consultasql->bindParam(':imagen', noticia::getImagen());
      $consultasql->bindParam(':pieimagen', noticia::getPieimagen());
      $consultasql->bindParam(':observaciones', noticia::getObservaciones());
      $consultasql->bindParam(':categoria', noticia::getCategoria());
      $consultasql->bindParam(':tags', noticia::getTags());
      $consultasql->bindParam(':fuente', noticia::getFuente());



      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al crear una noticia</p></script>";
        print $e->getMessage();
        exit();
      }
      noticia::setIdnoticia($conexion_db->lastInsertId());
    }
  }

  public static function borrarNoticia($idnoticia) {

    $conectar = new Conexion();

    $conexion_db = $conectar->conexion();

    $consultasql = $conexion_db->prepare('DELETE FROM noticias WHERE idnoticia="' . $idnoticia . '"');

    $consultasql->bindParam(':idnoticia', noticia::getIdnoticia());

    $consultasql->execute();
    //    echo "La noticia: ".$idnoticia."ha sido borrada";


    // Comprobar que la noticia ha sido borrada NO FUNCIONA AÚN
    
    
    if($consultasql->execute()) {

      echo "<br> La noticia: " . $idnoticia . " ha sido borrada";
    } else {
      print"<br> Error al borrar la noticia " . $idnoticia;
    }
  }

// Cierro Borrar noticia


  /* C I E R R O    O P C I O N   (A) M V C
   */






  // echo "Había que desconecar";
  // self::desconectar($conexion_db);


  /*   O P C I O N     ( B ) sin M V C




    $consultasql = " INSERT INTO  noticias (idnoticia, titular, fechanoticia, subtitulo, entradilla, cuerpo, imagen, pieimagen, observaciones, categoria, tags, fuente)
    VALUES (:idnoticia, :titular, :fechanoticia, :subtitulo, :entradilla, :cuerpo, :imagen, :pieimagen, :observaciones, :categoria, :tags, :fuente) ";



    $resultado = $conexion_db->prepare($consultasql);

    if ($resultado->execute(array(":idnoticia" => $idnoticia, ":titular" => $titular, ":fechanoticia" => $fechanoticia, ":subtitulo" => $subtitulo,
    ":entradilla" => $entradilla, ":cuerpo" => $cuerpo, ":imagen" => $imagen, ":pieimagen" => $pieimagen, ":observaciones" => $observaciones,
    ":categoria" => $categoria, ":tags" => $tags, ":fuente" => $fuente))) {
    print "<br> La noticia se ha creado correctamente";
    } else {
    print "<br> Error al crear la noticia";
    }
   *  C I E R R R O   O P C I O N  ( B ) 
   */


// Cierro Clase  noticia
}

// Cierro Clase 
?>
