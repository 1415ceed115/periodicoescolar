<?php

require "../c/conexion.php";

class MvNoticia {

  public function mvNoticia() {


    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    // parent::__construct();
  }

  // Metodo para pedir una consulta y devolver registros
  public function get_noticias($datoid) {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // La variable query extiende de la superclase Conexion
    // si fuera con mysqli
    //$resultado = $conex_db->query('SELECT * FROM noticias WHERE idnoticia="'.$datoid.'"');
    // Hago un prepare, para preparar la ejecución sql

    $consulta = $conexion_db->prepare('SELECT * FROM noticias WHERE idnoticia="' . $datoid . '"');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $noticias = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $noticias; // Nos devuelve el array 
  }

  // Cierro constructor
//*

  public static function crearNoticia() {

    //$db = self::conectar();

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    //  if (self::buscarUsuariosTF(Usuario::getIdnoticia())) /* Modifica */ {
    if (self::get_noticias(get_noticias::getIdnoticia())) /* crea */ {
      $consultasql = $conexion_db->prepare(" INSERT INTO  noticias (idnoticia, titular, fechanoticia, subtitulo, entradilla, cuerpo, imagen, pieimagen, observaciones, categoria, tags, fuente)
                        VALUES ('$idnoticia', '$titular', '$fechanoticia', '$subtitulo', '$entradilla', '$cuerpo', '$imagen', '$pieimagen', '$observaciones',
              '$categoria', '$tags','$fuente') ");
      $consultasql->bindParam(':idnoticia', noticia::getIdnoticia());
      $consultasql->bindParam(':titular', Usuario::getTitular());
      $consultasql->bindParam(':fechanoticia', Usuario::getFechanoticia());
      $consultasql->bindParam(':subtitulo', Usuario::getSubtitulo());
      $consultasql->bindParam(':entradilla', Usuario::getEntradilla());
      $consultasql->bindParam(':cuerpo', Usuario::getCuerpo());
      $consultasql->bindParam(':imagen', Usuario::getImagen());
      $consultasql->bindParam(':pieimagen', Usuario::getPieimagen());
      $consultasql->bindParam(':observaciones', Usuario::getObservaciones());
      $consultasql->bindParam(':categoria', Usuario::getCategoria());
      $consultasql->bindParam(':tags', Usuario::getTags());
      $consultasql->bindParam(':fuente', Usuario::getFuente());


      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>error al crear una noticia</p></script>";
        print $e->getMessage();
        exit();
      }
    } else /* Inserta */ {
      //if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
      //    echo "Wiii, tengo CRYPT_BLOWFISH!";
      //}
      // Al utilizar este metodo de encriptacion no se puede visualizar al usuario su contraseña almacenada, a no ser que se guarde la semilla.
      // Generamos un salt aleatoreo, de 22 caracteres para Bcrypt
      //$salt = substr(base64_encode(openssl_random_pseudo_bytes('30')), 0, 22);
      // A Crypt no le gustan los '+' así que los vamos a reemplazar por puntos.
      //$salt = strtr($salt, array('+' => '.'));
      // Generamos el hash
      //$hash = crypt(Usuario::getpassword(), '$2y$10$' . $salt);
      //$password_hash = crypt(Usuario::getpassword());
      $consulta = $db->prepare('INSERT INTO usuarios (dni, nombre, email, contrasena, idRol, cif_transportista, cif_cliente) VALUES(:dni, :nombre, :correo, :pa, :rol, :tra, :cli)');
      $consulta->bindParam(':dni', Usuario::getdni());
      $consulta->bindParam(':nombre', Usuario::getnombre());
      $consulta->bindParam(':correo', Usuario::getemail());
      $consulta->bindParam(':pa', Usuario::getpassword());
      //$consulta->bindParam(':passw', $hash);
      $consulta->bindParam(':rol', Usuario::getnivelSeguridad());
      $consulta->bindParam(':tra', Usuario::getcifTransportista());
      $consulta->bindParam(':cli', Usuario::getncifCliente());
      try {
        $consulta->execute();
      } catch (PDOException $e) {
        print "<script><p>error al dar de alta un usuario</p></script>";
        print $e->getMessage();
        exit();
      }
      Usuario::setdni($db->lastInsertId());
    }
    self::desconectar($db);
  }

}

// Cierro Clase 
?>
