<?php

//require "../c/conexion.php";
require '../m/mOcio.php';

class MvOcio {

  public function mvOcio() {


    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    // parent::__construct();
  }

  // Metodo para pedir una consulta y devolver registros
  public static function get_ocios($semana) {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    $consulta = $conexion_db->prepare('SELECT * FROM ocio WHERE semana="' . $semana . '"');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $ocio = $consulta->fetchAll(PDO::FETCH_ASSOC); //ocios??
    return $ocio; // Nos devuelve el array 

    /*
      return new noticias($datoarray['idnoticia'],$datoarray['titular'],$datoarray['fechanoticia'],$datoarray['subtitulo'],
      $datoarray['entradilla'],$datoarray['cuerpo'],$datoarray['imagen'],$datoarray['pieimagen'],$datoarray['observaciones'],$datoarray['categoria']
      ,$datoarray['tags'],$datoarray['fuente']
      );
     */
  }

  // Cierro constructor
//*

  public static function crearOcio($semana) {


    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    //   O P C I O N     ( A )
    if (self::get_ocios($semana)) { // Actualiza si existe 
      //if (self::get_noticia(get_noticia::getIdnoticia())) {
      $consultasql = $conexion_db->prepare("UPDATE  ocio SET semana= :semana, tv= :tv, internet= :internet,
      espectaculos= :espectaculos, actividadesa= :actividades ");



      $consultasql->bindParam(':semana', Ocio::getSemana());
      $consultasql->bindParam(':tv', Ocio::getTv());
      $consultasql->bindParam(':internet', Ocio::getInternet());
      $consultasql->bindParam(':espectaculos', Ocio::getEspectaculos());
      $consultasql->bindParam(':actividades', Ocio::getActividades());



      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al Actualizar una semana de ocio</p></script>";
        print $e->getMessage();
        exit();
      }
    } else {  // Si no existe inserta noticia
      $consultasql = $conexion_db->prepare(" INSERT INTO  ocio (semana, tv, internet, espectaculos, actividades)
      VALUES (:semana, :tv, :internet, :espectaculos, :actividades) ");


      $consultasql->bindParam(':semana', Ocio::getSemana());
      $consultasql->bindParam(':tv', Ocio::getTv());
      $consultasql->bindParam(':internet', Ocio::getInternet());
      $consultasql->bindParam(':espectaculos', Ocio::getEspectaculos());
      $consultasql->bindParam(':actividades', Ocio::getActividades());




      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al crear una semana de ocio</p></script>";
        print $e->getMessage();
        exit();
      }
      ocio::setSemana($conexion_db->lastInsertId());
    }
  }

  public static function borrarOcio($semana) {

    $conectar = new Conexion();

    $conexion_db = $conectar->conexion();

    $consultasql = $conexion_db->prepare('DELETE FROM ocio WHERE semana="' . $semana . '"');

    $consultasql->bindParam(':semana', ocio::getSemana());

    $consultasql->execute();
    //    echo "La semana de ocio: ".$semana."ha sido borrada";
    // Comprobar que la Semana de Ocio ha sido borrada NO FUNCIONA AÚN


    if ($consultasql->execute()) {

      echo "<br> La semana de ocio: " . $semana . " ha sido borrada";
    } else {
      print"<br> Error al borrar la semana de ocio " . $semana;
    }
  }

// Cierro Borrar Ocio


  /* C I E R R O    O P C I O N   (A) M V C
   */






  // echo "Había que desconecar";
  // self::desconectar($conexion_db);


  /*   O P C I O N     ( B ) sin M V C




    $consultasql = " INSERT INTO  noticias (idnoticia, titular, fechanoticia, subtitulo, entradilla, cuerpo, imagen, pieimagen, observaciones, categoria, tags, fuente)
    VALUES (:idnoticia, :titular, :fechanoticia, :subtitulo, :entradilla, :cuerpo, :imagen, :pieimagen, :observaciones, :categoria, :tags, :fuente) ";



    $resultado = $conexion_db->prepare($consultasql);

    if ($resultado->execute(array(":idnoticia" => $idnoticia, ":titular" => $titular, ":fechanoticia" => $fechanoticia, ":subtitulo" => $subtitulo,
    ":entradilla" => $entradilla, ":cuerpo" => $cuerpo, ":imagen" => $imagen, ":pieimagen" => $pieimagen, ":observaciones" => $observaciones,
    ":categoria" => $categoria, ":tags" => $tags, ":fuente" => $fuente))) {
    print "<br> La noticia se ha creado correctamente";
    } else {
    print "<br> Error al crear la noticia";
    }
   *  C I E R R R O   O P C I O N  ( B ) 
   */


// Cierro Clase  noticia
}

// Cierro Clase 
?>
