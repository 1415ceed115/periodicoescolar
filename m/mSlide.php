<?php

class Slide {

// Defino las private variables del slide
private static $idslide;
private static $s_idnoticia;
private static $s_titular;
private static $s_imagen;

// Defino el metodo constructor hago como un set inicial para no dejar vacio y evitar errores

public function __construct($idslide = null, $s_idnoticia = null, $s_titular = null, $s_imagen = NULL) {


self::$idslide = $idslide;
self::$s_idnoticia = $s_idnoticia;
self::$s_titular = $s_titular;
self::$s_imagen = $s_imagen;
}

// Metodo getter  ponemos self::$variable en vez de this por trajabar con objetos

public static function getIdslide() {
return self::$idslide;
}

public static function getS_idnoticia() {
return self::$s_idnoticia;
}

public static function getS_titular() {
return self::$s_titular;
}

public static function getS_imagen() {
return self::$s_imagen;
}


public static function setIdslide($idslide) {
self::$idslide = $idslide;
}

public static function setS_idnoticia($s_idnoticia) {
self::$s_idnoticia = $s_idnoticia;
}

public static function setS_titular($S_titular) {
self::$s_titular= $S_titular;
}

public static function setS_imagen($s_imagen) {
self::$s_imagen = $s_imagen;
}










/*
  // Metodo para pedir una consulta y devolver registros
  public function get_noticias($datoid) {


  // La privateiable query extiende de la superclase Conexion
  $resultado = $this->conexion_db->query('SELECT * FROM noticias WHERE idnoticia="' . $datoid . '"');
  $noticias = $resultado->fetch_all(MYSQLI_ASSOC);

  return $noticias; // Nos devuelve el array
  }
 */


}
// Cierro Clase 
?>
