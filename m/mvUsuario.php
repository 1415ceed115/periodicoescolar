<?php

//require '../c/conexion.php';
require '../m/mUsuario.php';

class MvUsuario {

  public function mvUsuario() {


    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    // parent::__construct();
  }

  // Metodo para pedir una consulta y devolver registros
  static public function get_usuarios($datodni) {

    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    // Hago un prepare, para preparar la ejecución sql

    $consulta = $conexion_db->prepare('SELECT * FROM usuarios WHERE dni="' . $datodni . '"');

    // Ejecuto la instruccion sql
    $consulta->execute();


    // Transformo el resultado de la consulta en un array asociativo
    $usuarios = $consulta->fetchAll(PDO::FETCH_ASSOC);

    return $usuarios; // Nos devuelve el array 

    /*
      return new noticias($datoarray['idnoticia'],$datoarray['titular'],$datoarray['fechanoticia'],$datoarray['subtitulo'],
      $datoarray['entradilla'],$datoarray['cuerpo'],$datoarray['imagen'],$datoarray['pieimagen'],$datoarray['observaciones'],$datoarray['categoria']
      ,$datoarray['tags'],$datoarray['fuente']
      );
     */
  }

  // Cierro constructor
//*

  public static function crearUsuario($dni) {


    $conectar = new Conexion();
    $conexion_db = $conectar->conexion();

    //   O P C I O N     ( A )
    if (self::get_usuarios($dni)) { // Actualiza si existe
      //if (self::get_noticia(get_noticia::getIdnoticia())) {
      
   
      $consultasql = $conexion_db->prepare("UPDATE  usuarios SET dni= :dni, nombre= :nombre, apellidos= :apellidos, email=:email,
      twitter= :twitter, telefono= :telefono, direccion= :direccion, poblacion= :poblacion, provincia= :provincia, codigopostal= :codigopostal,
      pais= :pais, permiso= :permiso, psw= :psw WHERE dni=:dni");


      
      $consultasql->bindParam(':dni', usuario::getDni());
      $consultasql->bindParam(':nombre', usuario::getNombre());
      $consultasql->bindParam(':apellidos', usuario::getApellidos());
      $consultasql->bindParam(':email', usuario::getEmail());
      $consultasql->bindParam(':twitter', usuario::getTwitter());
      $consultasql->bindParam(':telefono', usuario::getTelefono());
      $consultasql->bindParam(':direccion', usuario::getDireccion());
      $consultasql->bindParam(':poblacion', usuario::getPoblacion());
      $consultasql->bindParam(':provicia', usuario::getProvincia());
      $consultasql->bindParam(':codigopostal', usuario::getCodigopostal());
      $consultasql->bindParam(':pais', usuario::getPais());
      $consultasql->bindParam(':permiso', usuario::getPermiso());
      $consultasql->bindParam(':psw', usuario::getPsw());


      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al Actualizar un usuario</p></script>";
        print $e->getMessage();
        exit();
      }
    } else {  // Si no existe inserta usuario
      $consultasql = $conexion_db->prepare(" INSERT INTO  noticias (dni, nombre, apellidos, email, twitter, telefono, direccion, poblacion, provincia, codigopostal, pais, permiso, psw )
      VALUES (:dni, :nombre, :apellidos, :email, :twitter, :telefono, :direccion, :poblacion, :provincia, :codigopostal, :pais, :permiso, :psw ) ");

      $consultasql->bindParam(':dni', usuario::getDni());
      $consultasql->bindParam(':nombre', usuario::getNombre());
      $consultasql->bindParam(':apellidos', usuario::getApellidos());
      $consultasql->bindParam(':email', usuario::getEmail());
      $consultasql->bindParam(':twitter', usuario::getTwitter());
      $consultasql->bindParam(':telefono', usuario::getTelefono());
      $consultasql->bindParam(':direccion', usuario::getDireccion());
      $consultasql->bindParam(':poblacion', usuario::getPoblacion());
      $consultasql->bindParam(':provicia', usuario::getProvincia());
      $consultasql->bindParam(':codigopostal', usuario::getCodigopostal());
      $consultasql->bindParam(':pais', usuario::getPais());
      $consultasql->bindParam(':permiso', usuario::getPermiso());
      $consultasql->bindParam(':psw', usuario::getPsw());



      try {
        $consultasql->execute();
      } catch (PDOException $e) {
        print "<script><p>Error al crear un usuario</p></script>";
        print $e->getMessage();
        exit();
      }
      usuario::setDni($conexion_db->lastInsertId());
    }
  }

  public static function borrarUsuario($dni) {

    $conectar = new Conexion();

    $conexion_db = $conectar->conexion();

    $consultasql = $conexion_db->prepare('DELETE FROM usuarios WHERE dni="' . $dni . '"');

 
    $consultasql->bindParam(':dni', usuario::getDni());

    $consultasql->execute();
    //    echo "El usuario: ".$dni."ha sido borrado";
    // Comprobar que el usuario ha sido borrado NO FUNCIONA AÚN


    if ($consultasql->execute()) {

      echo "<br> El usuario: " . $dni . " ha sido borrado";
    } else {
      print"<br> Error al borrar el usuario " . $dni;
    }
  }

// Cierro Borrar Usuario


  /* C I E R R O    O P C I O N   (A) M V C
   */






  // echo "Había que desconecar";
  // self::desconectar($conexion_db);


  /*   O P C I O N     ( B ) sin M V C




    $consultasql = " INSERT INTO  noticias (idnoticia, titular, fechanoticia, subtitulo, entradilla, cuerpo, imagen, pieimagen, observaciones, categoria, tags, fuente)
    VALUES (:idnoticia, :titular, :fechanoticia, :subtitulo, :entradilla, :cuerpo, :imagen, :pieimagen, :observaciones, :categoria, :tags, :fuente) ";



    $resultado = $conexion_db->prepare($consultasql);

    if ($resultado->execute(array(":idnoticia" => $idnoticia, ":titular" => $titular, ":fechanoticia" => $fechanoticia, ":subtitulo" => $subtitulo,
    ":entradilla" => $entradilla, ":cuerpo" => $cuerpo, ":imagen" => $imagen, ":pieimagen" => $pieimagen, ":observaciones" => $observaciones,
    ":categoria" => $categoria, ":tags" => $tags, ":fuente" => $fuente))) {
    print "<br> La noticia se ha creado correctamente";
    } else {
    print "<br> Error al crear la noticia";
    }
   *  C I E R R R O   O P C I O N  ( B ) 
   */


// Cierro Clase  noticia
}

// Cierro Clase 
?>
