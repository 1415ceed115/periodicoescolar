<?php

class Ocio {

  // Defino las private variables de Ocio
  private static $semana;
  private static $tv;
  private static $internet;
  private static $espectaculos;
  private static $actividades;

  // Defino el metodo constructor
  // Defino el metodo constructor hago como un set inicial para no dejar vacio y evitar errores

  public function __construct($semana = null, $tv = null, $internet = NULL, $espectaculos = NULL, $actividades = NULL
  ) {
    self::$semana = $semana;
    self::$tv = $tv;
    self::$internet = $internet;
    self::$espectaculos = $espectaculos;
    self::$actividades = $actividades;
  }

  // Cierro constructor
  // Metodo getter  ponemos self::$variable en vez de this por trajabar con objetos


  public static function getSemana() {
    return self::$semana;
  }

  public static function getTv() {
    return self::$tv;
  }

  public static function getInternet() {
    return self::$internet;
  }

  public static function getEspectaculos() {
    return self::$espectaculos;
  }

  public static function getActividades() {
    return self::$actividades;
  }

  // Metodos Setter



  public static function setSemana($semana) {
    self::$semana = $semana;
  }

  public static function setTv($tv) {
    self::$tv = $tv;
  }

  public static function setInternet($internet) {
    self::$internet = $internet;
  }

  public static function setEspectaculos($espectaculos) {
    self::$espectaculos = $espectaculos;
  }

  public static function setActividades($actividades) {
    self::$actividades = $actividades;
  }

  /*
    // Metodo para pedir una consulta y devolver registros
    public function get_noticias($semana) {


    // La privateiable query extiende de la superclase Conexion
    $resultado = $this->conexion_db->query('SELECT * FROM ocio WHERE semana="' . $semana . '"');
    $noticias = $resultado->fetch_all(MYSQLI_ASSOC);

    return $noticias; // Nos devuelve el array
    }
   */
}

// Cierro Clase 
?>
