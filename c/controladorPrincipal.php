<?php

require_once 'ControladorNoticia.php';
require_once 'ControladorUsuario.php';
//require_once 'ControladorOcio.php';
//require_once 'ControladorSlide.php';
//require_once 'Controladortwitter.php';
//require_once 'ControladorComentarios.php';

$arrayswitch = array(
    // v el valor recogido, controlador es la clase que va ejecutar y 
    // accion la funcion que voy a ejecutar
    'nv' => array('controlador' => 'ControladorNoticia', 'accion' => 'verNoticia'),
    'nc' => array('controlador' => 'ControladorNoticia', 'accion' => 'crearNoticia'),
    'nb' => array('controlador' => 'ControladorNoticia', 'accion' => 'borrarNoticia'),
    'uv' => array('controlador' => 'ControladorUsuario', 'accion' => 'verUsuario'),
    'uc' => array('controlador' => 'ControladorUsuario', 'accion' => 'crearUsuario'),
    'ub' => array('controlador' => 'ControladorUsuario', 'accion' => 'borrarUsuario'),
);


$valor = $_REQUEST ['accion'];

// switch para enviar a la página correspondiente
/*
 *  n  noticia
 *  nv ver  noticia
 *  nc crear noticia
 *  nb borrar noticia
 *  nm modificar noticia
 * 
 *  t  twitter
 *  tv ver  twitter
 *  tc crear twitter
 *  tb borrar twitter
 *  tm modificar twitter
 * 
 *  c  comentario
 *  cv ver  comentario
 *  cc crear comentario
 *  cb borrar comentario
 *  cm modificar comentario
 * 
 *  o  ocio
 *  ov ver  ocio
 *  oc crear ocio
 *  ob borrar ocio
 *  om modificar ocio
 * 
 *  u  usuario
 *  um ver  usuario
 *  vm crear usuario
 *  um borrar usuario
 *  um modificar usuario
 * 
 * 
 */

/*
  switch ($valor) {

  case "v":
  echo"Estas llamando al controlador noticia";

  break;
  case "t":
  echo"";
  break;

  case "c":
  echo"";
  break;

  case "o":
  echo"";
  break;

  case "u":
  echo"";
  break;

  default : echo "No hay nada seleccionado";

  }
 */


// Comprobar si hay valor en la variable accion si existe dentro del arrayswith

if (isset($_GET['accion'])) {
  if (isset($arrayswitch[$_GET['accion']])) {
    $ruta = $_GET['accion'];
  } else {
    header('Status: 404 Not Found');
    echo '<html><body><h1>Error 404: No existe la ruta <i>' . $_GET['accion'] . '</p></body></html>';
    exit;
  }
}


$controlador = $arrayswitch[$ruta];

// Ejecuto la clase y la funcion que tengo en el arrayswitch

if (method_exists($controlador['controlador'], $controlador['accion'])) {

  // la siguiente linea seria como: call_user_func(array (new Controller, 'insertar')), es decir instanciamos a la clase Controller la accion.

  call_user_func(array(new $controlador['controlador'], $controlador['accion']));
} else {

  header('Status: 404 Not Found');
  echo '<html><body><h1>Error 404: El controlador <i>' . $controlador['controlador'] . '->' . $controlador['accion'] . '</i> no existe</h1></body></html>';
}



/*
  if ($valor == 'v') {

  call_user_func(new ControladorNoticia, ControladorNoticia);
  } else {
  echo "ha fallado";
  }
 */
?>