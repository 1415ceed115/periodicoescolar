<?php

require "conexion.php";

class MuestraNoticias extends Conexion {

  public function devuelveNoticias() {


    // Llamo al constructor de la clase conexion para ejecutarlo y conectar a la BD
    parent::__construct();
  }

  // Metodo para pedir una consulta y devolver registros
  public function get_noticias($datoid) {

    
    // La variable query extiende de la superclase Conexion
    $resultado = $this->conexion_db->query('SELECT * FROM noticias WHERE idnoticia="'.$datoid.'"');
    $noticias = $resultado->fetch_all(MYSQLI_ASSOC);

    return $noticias; // Nos devuelve el array 
  }

  // Cierro constructor
}

// Cierro Clase 
?>
