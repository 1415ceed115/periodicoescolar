<?php

// Conexion objetos con PDO
 

require ('config.php');

class Conexion {

  //protected $conexion_db;

  // Metodo constructor de la conexion

  public function conexion() {

    try {
      // Para conectar
      $conexion_db = New PDO('mysql:host='.DB_HOST.';dbname='.DB_NOMBRE, DB_USER, DB_PSW);
    

      // Para capturar errores
      $conexion_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      // Para asignar utf8 a la base de datos
      $conexion_db->exec('SET NAMES utf8');
    } catch (PDOException $e) {

      print "Error al conectar con la base de datos:" . $e;
      die();
    }

   return $conexion_db;
  }
/*
   public static function desconectar($conexion) {
        $conexion = null;
    }
    
  */
  
  
}

/*
  // Conexion objetos con msqli


  require ('config.php');

  class Conexion{

  protected $conexion_db;

  // Metodo constructor de la conexion

  public function conexion(){

  $this->conexion_db= new mysqli(DB_HOST, DB_USER, DB_PSW, DB_NOMBRE);

  if ($this->conexion_db->connect_errno){

  echo "Fallo al conectar a Mysql".$this->conexion_db->connect_error;

  return;
  }

  $this->conexion_db->set_charset(DB_CHARSET);

  }


  }

 */


/*

  // CONEXION MYSQLI local



  $conexion = mysqli_connect("localhost", "u572505354_admin", "pe2016", "u572505354_pe");

  mysqli_set_charset($conexion,"utf8");







  if(!$conexion){

  echo "Error en la conexion";

  };


 */
?>
