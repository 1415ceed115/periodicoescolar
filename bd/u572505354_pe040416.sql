-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2016 a las 22:14:09
-- Versión del servidor: 5.6.26
-- Versión de PHP: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u572505354_pe`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE IF NOT EXISTS `comentarios` (
  `idcomentario` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `idnoticia` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `comentarista` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `comentario` varchar(144) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `fechacomentario` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `idnoticia` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `titular` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechanoticia` varchar(8) COLLATE utf8_spanish_ci DEFAULT NULL,
  `subtitulo` varchar(144) COLLATE utf8_spanish_ci DEFAULT NULL,
  `entradilla` varchar(144) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cuerpo` varchar(700) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pieimagen` varchar(144) COLLATE utf8_spanish_ci DEFAULT NULL,
  `observaciones` varchar(144) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `categoria` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tags` varchar(144) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fuente` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`idnoticia`, `titular`, `fechanoticia`, `subtitulo`, `entradilla`, `cuerpo`, `imagen`, `pieimagen`, `observaciones`, `categoria`, `tags`, `fuente`) VALUES
('10-01032016', 'Comienzan las fallas', '01/03/16', 'Primera mascletá', 'Primera mascletá a cargo de la pirotecnia caballer', 'La primera mascletá es ca cargo de la pirotecnia caballer, de Godella, se espera una afluencia de público de 50.000 personas', 'mascle010316.jpg', 'foto: J. Bleda', 'Noticia de pruebas', 'Cultura', 'ayuntamiento, mascleta', 'Propia'),
('10-14032016', 'El ninot indultat infantil es para Sueca-Literato Azorín', '14/0316', 'La figura inspirada en la llegada del circo ruso a Valencia muestra a una familia preparándose antes de la función', ' Hoy es el primer día de reparto de premios importantes de Fallas, que se prolongará hasta el día 16. En esta ocasión el ''ninot indultat'' infant', 'Hoy es el primer día de reparto de premios importantes de Fallas, que se prolongará hasta el día 16. En esta ocasión el ''ninot indultat'' infantil ha cumplido los pronósticos y será la figura de Sueca-Literato Azorín del artista Sergio Gómez, la que se salvará del fuego, tras obtener 8.627 votos.\r\n\r\nLa escena ''El circo...ohhh'' dentro de la temática de la falla, inspirada en la llegada del circo ruso a Valencia, trata sobre una familia circense que se encuentra preparándose en un camerino antiguo con suelo de cerámica para la función. El personaje principal representa a un padre pintando a su hijo que va vestido de león antes de comenzar el espectáculo.', 'ninot-indultat.jpg', NULL, NULL, 'Fallas', 'Fallas, ninot indultat,', 'Levante emv'),
('10-24032016', 'Semana Santa para devotos y turistas', '24/03/16', 'Comienzan las procesiones de Semana Santa 2016', 'Las cofradías del marítimo en pleno.', 'Las cofradías del marítimo de Valencia ayer tuvieron la primera procesión, con un gran número de devotos y turistas que adminaron la solemnidad de la misma.', 'semana santa.jpg', 'foto: EFE', 'Noticia de pruebas', 'Cultura', 'Semana Santa, cultura', 'Propia'),
('10-29032016', 'cicloturismo', '', '', '', '', 'cicloturismo.jpg', '', '', '', '', ''),
('20-01032016', 'Aula del futuro', '01/03/16', 'Samsung presenta aula del fututo', 'Samsung apuesta por la tecnología para la enseñanza.', 'Ésta se encuentra ubicada dentro del Colegio Ciudad de México campus Polanco, la empresa de tecnología anunció que con estos avances ayudarán a los jóvenes a aumentar su competitividad.\r\n\r\nEl aula cuenta con pizarrones interactivos, computadoras personales, tablets e impresoras móviles de última generación.\r\n\r\nHéctor Jung quien es Presidente de Samsung en México puntualizó “Este paso representa una parte fundamental en nuestros esfuerzos por contribuir a la digitalización de los salones de clase y elevar los niveles de educación.', 'aulafuturo.jpg', 'foto: J. Bleda', 'Noticia de pruebas', 'educación', 'educación, cultura, tecnología, samsung', 'http://technomexico.com'),
('20-28032016', 'Un mirador entre secuoyas', '28/03/16', 'Las valencianas Carla Herranz y Carla Garzó ganan el concurso internacional de arquitectura Rethinking Competitions con su proyecto ', 'entradilla', 'Teníamos que escalar en un bosque de secuoyas y se nos ocurrió inventarnos un mirador diferente». Carla Herranz (Port de Sagunt, 1991) y Carla Garzó (Valencia, 1991) han sido las ganadoras del Rethinking Competitions, un concurso internacional de arquitectura en el que decidieron participar con una idea peculiar y cuyo premio asciende a 3.000 euros. Partían de la premisa de que todos los proyectos tuvieran que ver con «escalar en un bosque mediante una estructura arquitectónica» y contaban con libertad para crear sin límites.  Así, sin más, nació Blub!: un globo transparente lleno de helio con una esfera en su interior acolchada donde cabe una persona.', 'secuoyas.jpg', 'Foto : wikipedia', 'Noticia de Pruebas', 'Cultura', 'Cultura, naturaleza, valencia', 'Levante'),
('30-01032016', 'Proyecto SOLE en Fitur', '01/03/16', 'Una representación del CEED, viajó a Fitur para presentar el proyecto SOLE.', 'Se tuvo la oportunidad de dialogar con el INVATTUR y trazar proyectos de colaboración entre ambas instituciones.', 'Se presentó en FITUR el proyecto europeo SOLE (Social Open Learning Environment) que coordina el CEED\r\n\r\nEn el espacio Know-How & Export y con el apoyo de la Fundación Finnovaregio, Segittur (Innovación y Turismo) e Invattur (Instituto Valenciano de Tecnologías  Turísticas), el CEED tuvo ocasión de dar a conocer su red social SOLE en diferentes stands a un numeroso público asistente. SOLE tiene ahora la ocasión de abrir el espacio internacional  a emprendedores del campo del turismo para  que de este modo puedan dar a conocer su potencial, sus conocimientos, servicios y eventos turísticos.', 'sole.png', 'foto: ceed.', 'Noticia de pruebas', 'educación', 'educación, cultura, turismo', 'http://ceedcv.org'),
('40-01032016', 'Exámenes finales DAW', '01/03/16', 'Como afrontar el estrés de los exámenes finales.', 'Como afrontar el estrés de los exámenes finales y la carga del proyecto y las prácticas en empresas.', 'El estrés es  parte de la vida de un estudiante. De hecho, cualquier estudiante que se preocupa por los exámenes sufre de algún tipo de estrés académico. Sin embargo, debes recordar que el estrés existe por un motivo y que tú eres quién elige si quieres que te afecte negativamente o que te ayude a mejorar tu estudio.\r\n\r\nPara combatir el estrés primero debes entender las razones que lo causan para así establecer métodos que te ayuden a sentirte mejor cuando éste se presenta. Diversos estudios han puesto de manifiesto algunas explicaciones comunes del estrés académico:\r\n\r\n- Falta de planificación y organización\r\n- Presiones externas por alcanzar buenos resultados\r\n- Alta competitividad.', 'examenes.jpg', 'foto: google.', 'Noticia de pruebas', 'educación, e', 'educación, estres, examenes', 'www.examtime.com'),
('99-28032016', 'Un mirador entre secuoyas', '28/03/20', 'Las valencianas Carla Herranz y Carla Garzó ganan el concurso internacional de arquitectura Rethinking Competitions con su proyecto ', 'entradilla', '  laura julián | valencia «Teníamos que escalar en un bosque de secuoyas y se nos ocurrió inventarnos un mirador diferente». Carla Herranz (Port de Sagunt, 1991) y Carla Garzó (Valencia, 1991) han sido las ganadoras del Rethinking Competitions, un concurso internacional de arquitectura en el que decidieron participar con una idea peculiar y cuyo premio asciende a 3.000 euros. Partían de la premisa de que todos los proyectos tuvieran que ver con «escalar en un bosque mediante una estructura arquitectónica» y contaban con libertad para crear sin límites.  Así, sin más, nació Blub!: un globo transparente lleno de helio con una esfera en su interior acolchada donde cabe una persona con posibilid', 'secuoyas.jpg', 'Foto : Pepico', 'Noticia de Pruebas', 'Cultura', 'Cultura, naturaleza, valencia', 'Levante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ocio`
--

CREATE TABLE IF NOT EXISTS `ocio` (
  `semana` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tv` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `internet` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `espectaculos` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `actividades` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ocio`
--

INSERT INTO `ocio` (`semana`, `tv`, `internet`, `espectaculos`, `actividades`) VALUES
('1316', 'Documentales de la 2. 16h de lunes a viernes.\r\nOrbita Laika. Domingos 23h en la 2.', 'Falleciemiento del invetor de la arroba @.', 'Teatre Talia. La Alquería Blanca. Viernes Sábado y domingo 20:30h. ', 'Excursión a la sierra de Mariola. organizada por el centro botánico de Valencia.'),
('1016', 'Grandes documetales de la 2. De Lunes a Viernes a las 16:25h. Esta semana con el Tejón de la miel, Bahías de Australia cómo más interesantes.', 'Lanzamiento del nuevos productos de Apple. consulta su web.', 'Estrenos de cine, si te has quedado en la ciudad es buena recomendación pasar por el cine, recomendamos SPOTLIGHT  y Serie Divergente: Leal', 'Pásete por la semana santa marinera del marítimo devoción y turismo. mira el programa en la web: http://www.semanasantamarinera.org/'),
('1216', 'Grandes documetales de la 2. De Lunes a Viernes a las 16:25h. Esta semana con el Tejón de la miel, Bahías de Australia cómo más interesantes.', 'Lanzamiento del nuevos productos de Apple. consulta su web.', 'Estrenos de cine, si te has quedado en la ciudad es buena recomendación pasar por el cine, recomendamos SPOTLIGHT  y Serie Divergente: Leal', 'Pásete por la semana santa marinera del marítimo devoción y turismo. mira el programa en la web: http://www.semanasantamarinera.org/'),
('1416', 'BIg Ban', 'xakata', 'cine', 'excursion al campo'),
('1516', 'BIg Ban', 'xakata', 'cine', 'excursion al campo'),
('1616', 'BIg Ban', 'xakata', 'cine', 'excursion al campo'),
('1116', 'Grandes documetales de la 2. De Lunes a Viernes a las 16:25h. Esta semana con el Tejón de la miel, Bahías de Australia cómo más interesantes.', 'Lanzamiento del nuevos productos de Apple. consulta su web.', 'Estrenos de cine, si te has quedado en la ciudad es buena recomendación pasar por el cine, recomendamos SPOTLIGHT  y Serie Divergente: Leal', 'Pásete por la semana santa marinera del marítimo devoción y turismo. mira el programa en la web: http://www.semanasantamarinera.org/'),
('1716', 'BIg Ban', 'xakata', 'cine', 'excursion al campo'),
('1816', 'BIg Ban', 'xakata', 'cine', 'excursion al campo'),
('1916', 'BIg Ban', 'xakata', 'cine', 'excursion al campo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slide`
--

CREATE TABLE IF NOT EXISTS `slide` (
  `idslide` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `s_idnoticia` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `s_titular` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `s_imagen` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `slide`
--

INSERT INTO `slide` (`idslide`, `s_idnoticia`, `s_titular`, `s_imagen`) VALUES
('s01', '10-01032016', 'Comienzan las fallas', 'mascle010316.jpg'),
('s02', '10-24032016', 'Semana Santa para devotos y turistas', 'semanasanta.jpg'),
('s03', '20-28032016', 'Un mirador entre secuoyas', 'secuoyas.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `twitter`
--

CREATE TABLE IF NOT EXISTS `twitter` (
  `t_idnoticia` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `idtwitter` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `usuariotw` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `twitt` varchar(144) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechatwitt` varchar(8) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `dni` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `twitter` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` int(9) DEFAULT NULL,
  `direccion` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `poblacion` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `provincia` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigopostal` int(5) DEFAULT NULL,
  `pais` varchar(30) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `permiso` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `psw` varchar(12) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`dni`, `nombre`, `apellidos`, `email`, `twitter`, `telefono`, `direccion`, `poblacion`, `provincia`, `codigopostal`, `pais`, `permiso`, `psw`) VALUES
('123456', 'Pepa', 'Perez', 'pepap@jmail.com', '@pepa', 96123456, 'C/ Mayor 5', 'Valencia', 'Valencia', 46002, 'Españá', '1', '123'),
('78910', 'Pepe', 'García', 'pgarcia@jmail.com', '@pgarcia', 96123456, 'C/ Menor, 2', 'Valencia', 'Valencia', 46001, 'España', '2', '123');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`idcomentario`),
  ADD KEY `idnoticia` (`idnoticia`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idnoticia`),
  ADD UNIQUE KEY `idnoticia` (`idnoticia`);

--
-- Indices de la tabla `ocio`
--
ALTER TABLE `ocio`
  ADD UNIQUE KEY `semana` (`semana`);

--
-- Indices de la tabla `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`idslide`),
  ADD KEY `idnoticia` (`s_idnoticia`);

--
-- Indices de la tabla `twitter`
--
ALTER TABLE `twitter`
  ADD PRIMARY KEY (`idtwitter`),
  ADD KEY `idnoticia` (`t_idnoticia`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD UNIQUE KEY `dni` (`dni`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `twitter`
--
ALTER TABLE `twitter`
  ADD CONSTRAINT `twitter_ibfk_1` FOREIGN KEY (`t_idnoticia`) REFERENCES `noticias` (`idnoticia`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
