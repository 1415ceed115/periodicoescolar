# Robots de la web www.periodicoescolar.es
User-agent: *
Disallow:/c/
Disallow:/font/
Disallow:/bd/
Disallow:/c/
Disallow: /css/
Disallow:/js/
Disallow:/nbproject/
Disallow: /login.php
Sitemap: http://www.periodicoescolar.es/sitemap.xml