<!DOCTYPE >

<head>

  <title>Periódico Escolar</title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


  <!-- MSgDg0YO2wL-FmRvv_zIbPUsXFo -->
  <meta name="robots" content="index, follow" />

  <!--<meta name="p:domain_verify" content="bb50e9396f97c65cee229e2e5de6a001"/>-->

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="keywords" content="periodico escolar, plataforma multimedia, periodico digital, plataforma digital, plataforma multimedia, web para televidion " />
  <meta name="revisit-after" content="3 days" />
  <meta name="description" content="periodico escolar, plataforma multimedia, periodico digital, plataforma digital " />
  <meta name="keywords" content="periodico escolar, plataforma multimedia, periodico digital, plataforma digital, plataforma multimedia, web para televidion " />
  <meta name="geo.region" content="ES-V" />
  <meta name="geo.placename" content="ALBORAYA" />

  <meta name="geo.position" content="39.500995; -0.34948" />
  <meta name="ICBM" content="39.500995, -0.34948" />



  <meta name="dcterms.language" CONTENT="ES">
  <meta name="dcterms.source" CONTENT="http://periodicoescolar.es/">
  <meta name="dcterms.relation" CONTENT="http://periodicoescolar.es">
  <meta name="dcterms.title" CONTENT="Periodico Escolar, plataforma multimedia">
  <meta name="dcterms.subject" CONTENT="diseño web Juan Antonio Bleda">
  <meta name="dcterms.description" CONTENT="Periodito escolar es una plataforma multimedia creada por Juan A. Bleda, diseñada para periodicos digitales y televisiones ">

  <link rel="shortcut icon" type="image/png" href="images/favicon.png"/>



  <link href="css/materialize.css" rel="stylesheet"  type="text/css">


  <script src="js/jquery.min.js"></script>
  <script src="js/materialize.min.js"></script>



</head>

<body>
  <?php include_once("analyticstracking.php") ?>
  <div class="container">


    <?php
    // Llamo al menú de navegación

    include 'v/nav_i.php';
    //include 'c/z_cpruebas.php';
    //include 'ControladorPanelNoticias.php';
    ?>


    <br><br><br><br>


    <div>
      Div del Slide 


      <?php
      /*
        require 'c/ControladorSlide.php';
        require 'c/z_cpruebas.php';
        //include 'm/mvSlide.php';

        $slideprincipal = new ControladorSlide;

        $slideprincipal = $slideprincipal->verSlide();

       */
      ?>



    </div>



    <br><br>

    <div class="row s12">
      <div class="col s9">
        <div class="slider">
          <ul class="slides responsive-img">
            <li>
              <img class='responsive-img' src='imagenes/aulafuturo.jpg' alt='periodico escolar samsung'>
              <div class="caption bottom">
                <div class="btn blue">El aula del futuro</div>
              </div>
            </li>
            <li>
              <img class='responsive-img' src="imagenes/sole.png" alt='periodico escolar sole'>
              <div class="caption bottom">
                <div class="btn blue">Proyecto SOLE</div>
              </div>
            </li>
            <li>
              <img class='responsive-img' src="imagenes/examenes.jpg" alt='periodico escolar examenes'>
              <div class="caption center">
                <div class="btn blue large">Exámenes finales</div>
              </div>
            </li>
          </ul>
        </div>

      </div>

      <div class="col s3 divazul">
        <b>NOTICIAS RECOMENDADAS<br> Periódico escolar</b><br>
        <hr class="deep-orange-text text-darken-3"><br>
        Ceed Valencia - Jornadas de actividades - Levante emv<br> 
        Proyecto SOLE - Presentación en FITUR - Europa Press<br>
        Exámenes Finales - Cómo afrontar el estrés - Las provincias<br>
        Aula del Futuro - Por Samsung España - Xakata<br>
      </div>
    </div>

    <br><br><br>

<!-- Div del Panel Noticias-->
    <div>

      Panel de últimas noticias
      <?php
      
       /*include 'ControladorPanelNoticias.php';
      
      
      
        $panelnoticias = new ControladorPanelNoticias();
        $panelprincipal = $panelnoticias->verPanel();

      */
      ?>
   
      
    </div>

<!-- Cierro Div del Panel Noticias-->


    <br><br>


    <div class="row s12">

      <div class="col s4">
        <img src="imagenes/sole.png" class="responsive-img">
      </div>

      <div class="col s4">
        <img src="imagenes/examenes.jpg" class="responsive-img">
      </div>

      <div class="col s4">
        <img src="imagenes/aulafuturo.jpg" class="responsive-img">    
      </div>
    </div>

    <div class="row s12">

      <div class="col s4">
        <img src="imagenes/examenes.jpg" class="responsive-img">
      </div>
      <div class="col s4">
        <img src="imagenes/sole.png" class="responsive-img">
      </div>
      <div class="col s4">
        <img src="imagenes/aulafuturo.jpg" class="responsive-img">    
      </div>
    </div>





    <br><hr class="deep-orange-text text-darken-3"><br>






    <footer>

      Mapa web periódico escolar  <br> 
      <a href='index.php'>Inicio</a><br>
      <a href='v/contacto.php'>Contacto</a><br>
      <a href='V/ocio.php'>Ocio</a><br>
      <a href='v/tiempo.php'>Tiempo</a><br>
      <a href='v/directos.php'>Directos</a><br>
      <a href='v/busqueda.php'>Busqueda</a>

      <br><br>


      <a href='index.html'> || Juan Antonio Bleda @2016 | Periódico Escolar @2016 ||</a>
    </footer>


    <script>

      // Funcion para llamar que funcione el select, hay que tener la llamada a materialize.js
      $(document).ready(function () {
        $('select').material_select(); // Para llamar al select
        $('.datepicker').pickadate();  // Para llamar al datepicker- el calendario
        $('.dropdown-button').dropdown(); // Llamo al menu desplegable
        $(".button-collapse").sideNav(); // LLamo a la funcion menu hamburgesa
        $('.slider').slider(); // Llamo a la funcion del Slider

      });



    </script>

  </div>
</body>
</html>
